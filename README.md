# Cerebro UUID plugin

Simple plugin for getting a random UUID v4.

![](assets/usage.gif)

## Usage

Start your input with `uuid`. The keyboard shortcut `CONTROL+C` or selecting the item on the UI will copy the UUID to the clipboard.

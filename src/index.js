import { v4 as uuidv4 } from 'uuid'
import icon from '../assets/icon.png'

export const fn = ({ actions, display, term }) => {

  if (!term.startsWith('uuid'))
    return;

  const uuid = uuidv4()

  display({
    title: `Random UUID: ${uuid}`,
    subtitle: 'CTRL+C or ENTER copies to clipboard',
    icon,
    clipboard: uuid,
    onSelect: () => actions.copyToClipboard(uuid)
  })
}

export const name = 'uuid'
export const keyword = 'uuid'
